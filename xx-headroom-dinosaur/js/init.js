"use strict";

// https://wicky.nillia.ms/headroom.js/
var header = document.querySelector("header");
var options = {
    offset: 205, // Menu se zasune až po dosažení nastavené vzdálenosti v pixelech
    tolerance: 5, // Tolerance detekce posunu - při velmi pomalém posunu se menu nezasune
};
var headroom = new Headroom(header, options);
headroom.init();