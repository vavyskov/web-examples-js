'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    /**
     * Pole - číselné indexy, hranaté závorky
     */
    var zelenina = ["mrkev", "rajče", "květák"];
    console.log(zelenina);
    console.log(zelenina[1]);
    console.log("Počet položek pole 'zelenina': " + zelenina.length);



    zelenina.push("celer", "česnek");
    console.log(zelenina);



    for (var i = 0; i < zelenina.length; i++) {
        console.log("Index: " + i + ", hodnota: " + zelenina[i]);
    }



    function vypis(hodnota, index) {
        console.log("Index: " + index + ", hodnota: " + hodnota);
    }
    zelenina.forEach(vypis);



});
