// ES6+

const options = {
    // threshold: 0,
    // threshold: 1,
    // threshold: 0.5,
    // threshold: [0.25, 0.5, 0.75],
    rootMargin: "-20% 0px -20% 0px",
    //root: document.querySelector(".container") // Browser viewport as default
};

const observer = new IntersectionObserver(detect, options);

document.querySelectorAll('.square').forEach((i) => observer.observe(i));

function detect(element) {
    for (let e of element) {
        if (e.isIntersecting) {
            e.target.classList.add('fade-in')
        } else {
            e.target.classList.remove('fade-in')
        }
    }
}
