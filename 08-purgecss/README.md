# PurgeCSS

Dokummentace:
- https://www.npmjs.com/package/purgecss#getting-started
- https://purgecss.com/plugins/postcss.html#installation

Vývoj:
```
npm install
(node build.js)
npm run build
```

Vytvoření projektu:
```
npm init
npm install --save-dev purgecss
```