const { PurgeCSS } = require('purgecss');
const fs = require('fs');

const targetFolder = 'build';

/* Create target folder */
if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
}

/* Copy files */
fs.copyFileSync('src/favicon.ico', `${targetFolder}/favicon.ico`);
fs.copyFileSync('src/index.html', `${targetFolder}/index.html`);

/* PurgeCSS */
(async () => {
    const purgecss = await new PurgeCSS().purge({
        content: ['src/index.html'],
        css: ['src/style.css'],
    });

    //console.log(purgecss);
    //console.log(purgecss[0]);
    //console.log(purgecss[0].css);
    fs.writeFileSync(`${targetFolder}/style.css`, purgecss[0].css);
})();

(async () => {
    const purgecss = await new PurgeCSS().purge({
        content: ['src/index.html'],
        css: ['src/bootstrap.min.css'],
        fontFaces: true,
        keyframes: true,
        variables: true,
        // safelist: [/^bg/, /red$/],
        // rejected: true,
        // rejectedCss: true,
        // sourceMap: true,
    });

    fs.writeFileSync(`${targetFolder}/bootstrap.min.css`, purgecss[0].css);
})();
