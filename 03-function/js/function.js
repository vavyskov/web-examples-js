'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    /**
     * https://www.w3schools.com/js/js_function_definition.asp
     */


    /**
     * Běžná funkce (Function Declarations)
     */
    function mojeFunkceA(a, b) {
        return a * b;
    }
    // Výpis do testovacího nástroje prohlížeče
    console.log(
        // Spuštění (zavolání) funkce s parametry 3, 5
        mojeFunkceA(3, 5)
    );



    /**
     * Funkce jako literál (Function Expression)
     * - přiřazení anonymní funkce do proměnné
     * - případné pojmenování anonymní funkce má pouze lokální platnost uvnitř funkce (vhodné např. pro ladění nebo rekurzi)
     */
    //var mojeFunkceB = function mojeFunkceA(a, b) { return a * b };
    var mojeFunkceB = function (a, b) { return a * b };
    console.log(
        mojeFunkceB(4, 3)
    );



    /**
     * Funkce jako objekt (Function Constructor)
     * - deklarace pomocí konstruktoru
     */
    var mojeFunkceC = new Function("a", "b", "return a * b");
    console.log(
        mojeFunkceC(3, 7)
    );



    /**
     * Okamžitě vyvolaná anonymní funkce (Immediately Invoked Function Expression)
     * - řešení problémů s globálními proměnnými
     * - definice Modulů
     * - definice Jmenných prostorů (namespace)
     */
    (function () {
        var textA;
        textA = "Okamžitě vyvolaná anonymní funkce";
        console.log(textA);
    })();
    //console.log(textA); // Not defined



    /**
     * Samospustitelná anonymní funkce (Self-Executing Anonymous Function)
     * - přiřazení samospustitelná anonymní funkce do proměnné
     * - v promenné je uložena výsledná hodnota anonymní funkce
     */
    var mojePromenna = (function () {
        var textB;
        textB = "Samospustitelná anonymní funkce";
        return textB;
    })();
    //console.log(textB); // Not defined
    console.log(mojePromenna);



    /*
    // Deklarace funkce s parametry (argumenty) a, b
    function soucet(a, b) {
        var x; // ES5
        //let; // ES6
        x = a + b;
        alert("Součet čísel je " + x);
        console.log("Počet předaných parametrů: " + arguments.length);
        console.log("1. agrument: " + a);
        console.log("1. agrument: " + arguments[0]);
        console.log("2. agrument: " + b);
        console.log("2. agrument: " + arguments[1]);
        console.log("Počet definovaných parametrů: " + soucet.length);
    }
    // Spuštění (zavolání) funkce s parametry 7, 5, 3, 1
    soucet(7, 5, 3, 1);
    */

});
