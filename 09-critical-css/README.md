# Critical CSS

Dokummentace:
- https://web.dev/articles/extract-critical-css
- https://web.dev/articles/preload-critical-assets
- https://developer.chrome.com/docs/lighthouse/performance/render-blocking-resources/
- https://blog.logrocket.com/9-tricks-eliminate-render-blocking-resources/
- https://github.com/addyosmani/critical-path-css-tools

Služby:
- https://www.sitelocity.com/critical-path-css-generator
- https://jonassebastianohlsson.com/criticalpathcssgenerator/
- https://www.corewebvitals.io/tools/critical-css-generator

Nástroje:
- https://github.com/addyosmani/critical
- https://github.com/pocketjoso/penthouse
    - https://github.com/maliMirkec/acclaimed
- https://github.com/nystudio107/rollup-plugin-critical
- https://www.npmjs.com/package/vite-plugin-css-injected-by-js
- https://github.com/tbela99/critical
    - https://github.com/tbela99/critical-docker

Media query:
- https://www.npmjs.com/package/postcss-extract-media-query

Vývoj:
```
npm install
sudo npm link gulp
(gulp)
npm run build
```

Samostatné Gulp úlohy
```
gulp purgecss
gulp critical
```

Vytvoření projektu:
```
npm init
npm install --save-dev gulp gulp-purgecss critical fancy-log gulp-replace postcss gulp-postcss cssnano gulp-htmlmin
```