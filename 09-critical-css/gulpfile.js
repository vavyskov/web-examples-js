import gulp from 'gulp';
import purgecss from 'gulp-purgecss';
import {stream as critical} from 'critical';
import log from 'fancy-log';
import fs from 'fs';
import replace from 'gulp-replace';
import postcss from 'gulp-postcss';
// import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import htmlmin from 'gulp-htmlmin';

const targetFolder = 'build';

/* Create target folder */
if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
}

/* Copy files */
fs.copyFileSync('src/favicon.ico', `${targetFolder}/favicon.ico`);
// fs.copyFileSync('src/index.html', `${targetFolder}/index.html`);

/* Minify HTML */
gulp.task('html', () => {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ 
            // removeOptionalTags: true, // Note: BrowserSync works only if page has a body tag
            collapseWhitespace: true,
            // removeComments: true,
            // minifyJS: true,
            // minifyCSS: true,
         }))
        .pipe(gulp.dest(targetFolder))
        // .pipe(browserSync.stream());
})

/* PurgeCSS */
gulp.task('purgecss', () => {
    return gulp.src('src/**/*.css')
        .pipe(purgecss({
            content: ['src/**/*.html'],
            fontFaces: true,
            keyframes: true,
            variables: true,
        }))
        .pipe(gulp.dest(targetFolder))
})

/* PostCSS (Autoprefixer, CSSnano) */
gulp.task('postcss', () => {
    return gulp.src(`${targetFolder}/**/*.css`)
        .pipe(postcss([
            // autoprefixer({browsers: ['last 1 version']}),
            cssnano(
            // {
				// discardComments: {
					// removeAll: true
				// }
            // }
            ),
        ]))
        .pipe(gulp.dest(targetFolder))
})

// Generate & Inline Critical-path CSS
gulp.task('critical', () => {
    return gulp
        .src(`${targetFolder}/*.html`)
        .pipe(
        critical({
            base: `${targetFolder}/`,
            inline: true,
            css: [
                `${targetFolder}/bootstrap.min.css`,
                `${targetFolder}/style.css`
            ],
            // extract: true,
            width: 412,
            height: 823,
        })
        )
        .on('error', (err) => {
            log.error(err.message);
        })
        .pipe(gulp.dest(targetFolder));
});

/* https://web.dev/articles/defer-non-critical-css */
gulp.task('defer', () => {
    return gulp
        .src(`${targetFolder}/index.html`)
        // Replace only in <head>
        .pipe(replace(/<head>[\s\S]*?<\/head>/g, function(match) {
            match = match.replace(/rel="stylesheet"/g, 'rel="preload" as="style"');
            match = match.replace(/onload="this.media=/g, 'onload="this.onload=null;this.rel=\'stylesheet\';this.media=');
            return match;
        }))
        .pipe(gulp.dest(`${targetFolder}/`));
});

gulp.task('default', gulp.series(
    'html',
    'purgecss',
    'postcss',
    'critical',
    'defer',
));
