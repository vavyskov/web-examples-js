'use strict';

console.log("7A body-async.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("7B body-async.js: DOM (without externals objects) loaded");

});
