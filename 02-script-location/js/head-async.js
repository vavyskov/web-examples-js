'use strict';

console.log("3A head-async.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("3B head-async.js: DOM (without externals objects) loaded");

});
