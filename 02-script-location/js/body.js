'use strict';

console.log("5A body.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("5B body.js: DOM (without externals objects) loaded");

});
