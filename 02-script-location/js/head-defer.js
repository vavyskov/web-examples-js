'use strict';

console.log("2A head-defer.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("2B head-defer.js: DOM (without externals objects) loaded");

});
