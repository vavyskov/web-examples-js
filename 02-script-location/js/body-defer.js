'use strict';

console.log("6A body-defer.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("6B body-defer.js: DOM (without externals objects) loaded");

});
