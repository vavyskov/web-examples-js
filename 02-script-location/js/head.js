'use strict';

console.log("1A head.js");

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6

    console.log("1B head.js: DOM (without externals objects) loaded");

});
