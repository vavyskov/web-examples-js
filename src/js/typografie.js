// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    const body = document.querySelector('body');
    //const body = document.getElementsByTagName("body")[0];

    /**
     * Typography
     * =========================================================================
     */

    function nbsp() {

        //body.innerHTML = body.innerHTML.replace(/\b([kosuvzai]|do|ke) /gi, '$1&nbsp;');

        // Characters
        let char = [
            '[kosuvz]',
            'do|ke|ku|na|od|po|se|ve|za|ze|bez|buď|čím|nad|pod|při|pro|tím|před|přes',
            '[ai]',
            'ač|až|ba|či|že|leč|než|když|vždyť'
        ];
        // FixMe: Apostrophe [^'][kosuvz] "Here's a paragraph"
        // FixMe: Why do not work both buttons?
        // ToDo: Exclude tags <pre> <code> etc.
        // ToDo: Typopo - Napsat požadavek na možnost doplnění jednoslabičných předložek a spojek

        // Only whole words with a space in the end
        let pattern = '\\b(' + char.join('|') + ') ';

        // Find and replace
        body.innerHTML = body.innerHTML.replace(RegExp(pattern, 'gi'), '$1&nbsp;');
    }

    // User event
    // https://www.w3schools.com/js/js_htmldom_eventlistener.asp
    document.getElementById('demo').addEventListener('click', function () {
        nbsp();
    });

    /**
     * Typopo
     * =========================================================================
     */

    document.getElementById('fixtypos').addEventListener('click', function () {
        body.innerHTML = fixTypos(body.innerHTML, 'cs');
    });

});
