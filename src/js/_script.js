/**
 * Global scripts
 **/

// Global variable
let testGlobal = "testGlobal";
console.log(testGlobal);

/**
 * Closure scripts
 *
 * File-closure and Strict mode
 *
 * All JavaScript code MUST be declared inside a closure wrapping the whole file
 * and this closure MUST be in strict mode.
 */
(function () {
    "use strict";

    // Closure (locale) variable
    let testLocal = "testLocal";
    console.log(testLocal);

}());


testGlobal = "testGlobal - changed";
console.log(testGlobal);

testLocal = "testLocal - changed";
console.log(testLocal);



/*import jquery from "../../node_modules/jquery/dist/jquery.slim.min";
import popper from "../../node_modules/popper.js/dist/umd/popper.min";
import bootstrap from "../../node_modules/bootstrap/dist/js/bootstrap.min";

import cardFlip from "_card-flip.js";

export {
    jquery,
    popper,
    bootstrap,
    cardFlip
}*/


/*import jquery from "jquery";
import popper from "popper";
import bootstrap from "bootstrap";

import "_card-flip.js";

export {
    jquery,
    popper,
    bootstrap
}*/
