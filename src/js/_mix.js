// Variable
var window, document;

// Show dialog window
/*window.onload = function () {
    alert("Ahoj!");
};*/

// Typography
function nbsp() {
    var x = document.querySelector('body');
    x.innerHTML = x.innerHTML.replace(/ ([ai]|[kosuvz]|do|ke|na|od|po|se|ve|za|ze) /g, ' $1&nbsp;');
}
nbsp();

// Set active menu link
function activeMenuLink() {
    if (document.getElementById('navbar')) {
        var aObjects = document.getElementById('navbar').getElementsByTagName('a');
        for (var i = 0; i < aObjects.length; i++) {
            if (document.location.href.indexOf(aObjects[i].href) >= 0) {
                aObjects[i].classList.add('active');
                aObjects[i].innerHTML += '<span class="sr-only">(current)</span>';
                // Set active DropDown item
                var dObject = aObjects[i].parentElement.parentElement.querySelector('.dropdown-toggle');
                if (dObject) {
                    dObject.classList.add('active');
                }
            }
        }
    }
}
activeMenuLink();





/**
 * Modal settings
 */
const bodyClass = document.body.classList;

// LocalStorage
var localStorage;
const settings = JSON.parse(localStorage.getItem('settings')) || {};

// Initialize settings from localStorage
function loadValues() {
    if (localStorage.getItem('settings') !== null) {
        reset.removeAttribute('disabled');
    }
    if (settings.font) {
        document.getElementById('font').checked = true;
        bodyClass.add('font-monospace');
    }
    if (settings.style) {
        document.getElementById('style').elements['style'].value = settings.style;
        bodyClass.add('style-' + settings.style);
        if (settings.style === 'none') {
            cssToggle(false);
        }
    }
}

// Update localStorage
function updateStorage(element) {
    localStorage.setItem('settings', JSON.stringify(settings));

    if (Object.keys(settings).length === 0) {
        localStorage.removeItem('settings');
    }
    
    reset.removeAttribute('disabled');
}

// Reset settings and localStorage
const reset = document.getElementById('reset');
function resetSettings() {
    reset.setAttribute('disabled', '');
    
    bodyClass.remove('font-monospace', 'style-none', 'style-dark');
    
    document.getElementById('font').checked = false;
    document.getElementById('style').elements['style'].value = 'light';

    // Reset localStorage
    Object.keys(settings).forEach(key => delete settings[key]);
    localStorage.removeItem('settings');
}

// Font settings
const font = document.getElementById('font');
function fontToggle() {
    bodyClass.toggle('font-monospace');
//    if (font.checked === true) {
//        bodyClass.add('font-monospace');
//    } else {
//        bodyClass.remove('font-monospace');
//    }
    
    if (!settings.font) {
        settings.font = 'monospace';   
    } else {
        delete settings.font;
    }
    updateStorage(settings);
}

// Disable|Enable all CSS
function cssToggle(value) {
    var i;
    for (i = 0; i < document.styleSheets.length; i++) {
        void(document.styleSheets.item(i).disabled = !value);
    }
}

// Reset style
function resetStyle() {
    bodyClass.remove('style-none', 'style-light', 'style-dark');
    cssToggle(true);
}

// Style settings
const none = document.getElementById('style-none');
function styleNone() {
    resetStyle();
    bodyClass.add('style-none');  
    cssToggle(false);

    settings.style = 'none';
    updateStorage(settings);
}
const light = document.getElementById('style-light');
function styleLight() {
    resetStyle();

    delete settings.style;
    updateStorage(settings);
}
const dark = document.getElementById('style-dark');
function styleDark() {
    resetStyle();
    bodyClass.add('style-dark');  

    settings.style = 'dark';
    updateStorage(settings);
}

/**
 * Ready
 */
window.onload = function () {
    // Set font settings
    font.addEventListener('click', fontToggle);
    
    // Set style settings
    none.addEventListener('click', styleNone);
    light.addEventListener('click', styleLight);
    dark.addEventListener('click', styleDark);
    
    // Reset settings
    reset.addEventListener('click', resetSettings);
    
    // Load values from LocalStorage
    loadValues();
};





/**
 * File-closure and Strict mode
 *
 * All JavaScript code MUST be declared inside a closure wrapping the whole file
 * and this closure MUST be in strict mode.
 */
(function () {
    'use strict';

    

}());






// Include HTML
/*var request = new XMLHttpRequest();
request.open('GET', 'asset/include/settings.html', true);
request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
        var resp = request.responseText;
        document.querySelector('#navbar').innerHTML += resp;
    }
};
request.send();*/