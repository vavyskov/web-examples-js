"use strict";

var cards = document.querySelectorAll(".card-flip");
cards.forEach(function (card) {
    card.addEventListener("click", function () {
        card.classList.toggle("is-flipped");
    });
});
