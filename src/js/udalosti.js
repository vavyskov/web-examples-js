// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     *
     * Atribut
     * <button onclick="alert('Demo (HTML atribut)');">Demo (HTML atribut)</button>
     */

    // Viz HTML soubor

    /**
     *
     * Vlastnost
     * Umožňuje zaregistrovat pouze jednu funkci pro danou událost
     */
    let vlastnost = document.getElementById("vlastnost");

    vlastnost.onclick = function () {
        //alert("Demo (vlastnost elementů DOM)");
        console.log("Demo (vlastnost elementů DOM)");
    }

    /**
     *
     * Metoda
     * Vícenásobné použití této metody umožňuje zaregistrovat více funkcí pro jednu událost
     */

    let metoda = document.getElementById("metoda");

    function zpracujMetodu() {
        //alert("Demo (metoda addEventListener)");
        console.log("Demo (metoda addEventListener)");
    }
    function zpracujMetodu2() {
        //alert("Demo (metoda 2 addEventListener)");
        console.log("Demo (metoda 2 addEventListener)");
    }

    metoda.addEventListener("click", zpracujMetodu);
    metoda.addEventListener("click", zpracujMetodu2);

    // Pozice
    let pozice = document.getElementById("pozice");

    function zpracujPozici(e) {
        pozice.textContent = `Pozice: (x = ${e.clientX}, y = ${e.clientY})`;
    }

    window.addEventListener("click", zpracujPozici, true);


});
