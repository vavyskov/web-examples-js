// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';


    /**
     * Local Storage
     * =========================================================================
     */

    const body = document.querySelector('body');
    const demo = document.getElementById("demo");

    // Set body class
    function setClass() {
        // Get mode from local storage
        if (localStorage.getItem('mode') === 'dark') {
            body.classList.add('dark');
        } else {
            body.classList.remove('dark');
        }
    }
    setClass();

    // Click event
    //demo.onclick = function () {
    //};
    demo.addEventListener("click", function () {
        // Toggle mode
        localStorage.setItem('mode', localStorage.getItem('mode') === 'dark' ? 'light' : 'dark');

        // Set body class
        setClass();
    });

});
