// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     * Version 1.A
     * =========================================================================
     *//*

    // User event
    // https://www.w3schools.com/js/js_htmldom_eventlistener.asp
    document.getElementById('demo').addEventListener('click', function () {
        // Variables
        // https://www.w3schools.com/js/js_let.asp
        let hodina = new Date().getHours();
        let text;

        // Get text from current time
        // https://www.w3schools.com/js/js_if_else.asp
        if (hodina >= 22 || hodina < 6) {
            text = 'Dobrou noc';
        } else if (hodina < 10) {
            text = 'Dobré ráno';
        } else if (hodina < 12) {
            text = 'Dobré dopoledne';
        } else if (hodina < 18) {
            text = 'Dobré odpoledne';
        } else {
            text = 'Dobrý večer';
        }

        // Write text
        // https://www.w3schools.com/jsref/prop_html_innerhtml.asp
        // https://www.w3schools.com/jsref/prop_node_innertext.asp
        document.getElementById('osloveni').innerHTML = text;
    });

    /**
     * Version 1.B
     * =========================================================================
     *//*

    // Constants
    const demo = document.getElementById('demo');
    const osloveni = document.getElementById('osloveni');

    // Function
    // https://www.w3schools.com/js/js_functions.asp
    function pozdrav() {
        // Variables
        // https://www.w3schools.com/js/js_let.asp
        let hodina = new Date().getHours();
        let text;

        // Get text from current time
        // https://www.w3schools.com/js/js_if_else.asp
        if (hodina >= 22 || hodina < 6) {
            text = 'Dobrou noc';
        } else if (hodina < 10) {
            text = 'Dobré ráno';
        } else if (hodina < 12) {
            text = 'Dobré dopoledne';
        } else if (hodina < 18) {
            text = 'Dobré odpoledne';
        } else {
            text = 'Dobrý večer';
        }

        // Write text
        // https://www.w3schools.com/jsref/prop_html_innerhtml.asp
        // https://www.w3schools.com/jsref/prop_node_innertext.asp
        osloveni.innerHTML = text;
    }

    // User event
    // https://www.w3schools.com/js/js_htmldom_eventlistener.asp
    demo.addEventListener('click', pozdrav);

    /**
     * Version 2
     * =========================================================================
     */

    // Constants
    const demo = document.getElementById('demo');
    const osloveni = document.getElementById('osloveni');
    // Array vs Object
    // https://www.w3schools.com/js/js_arrays.asp
    const data = {
        // key: value
        6: 'Dobré ráno',
        10: 'Dobré dopoledne',
        12: 'Dobré odpoledne',
        18: 'Dobrý večer',
        22: 'Dobrou noc'
    };

    // Variables
    let hodina = new Date().getHours();
    let klic;



    // // Test
    // let testHour = 15;
    // console.log("Hour: " + testHour);
    // let testIntervalA = [6, 10, 12, 18, 22];
    // console.log(testIntervalA);
    // let testIntervalB = Object.keys(data);
    // console.log(testIntervalB);
    // let testFilter = testIntervalB.filter(x => x <= testHour);
    // console.log("Filter: " + testFilter);
    // let testKlic = Math.max.apply(null, testFilter);
    // console.log("Key: " + testKlic);



    // Get key by current time
    klic = Math.max.apply(null, Object.keys(data).filter(x => x <= hodina));
    //klic = Math.max(...Object.keys(data).filter(x => x <= hodina));
    klic < 6 ? klic = 22 : klic;

    // Write text
    // https://www.w3schools.com/js/js_functions.asp
    function pozdrav() {
        osloveni.innerHTML = data[klic];
    }

    // User event
    demo.addEventListener('click', pozdrav);

});
