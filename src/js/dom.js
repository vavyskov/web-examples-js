// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    // Titulek
    let title = document.title;
    console.log("Titulek: " + title);

    // Nadpis
    let nadpis = document.getElementById("nadpis").innerText;
    console.log("Nadpis: " + nadpis);

    // Nový prvek div
    let novyPrvek = document.createElement("div"); // Vytvořeno v paměti
    document.body.appendChild(novyPrvek); // Připojení k existujícímu prvku
    novyPrvek.setAttribute("id", "demo");

    // Vložení textového obsahu
    novyPrvek.innerText = "Prostý text";

    // Vložení HTML obsahu - verze A
    //novyPrvek.innerHTML = "<p>Text odstavce</p>";

    // Vložení HTML obsahu - verze B
    let odstavec = document.createElement("p");
    odstavec.innerText = "Text odstavce";
    novyPrvek.appendChild(odstavec);



    // Disabled attribute
    let el, atr;
    el = document.createElement("link");
    atr = document.createAttribute("data-pokus");
    el.setAttributeNode(atr);
    el.setAttribute("id", "test");
    //atr.nodeValue = "foo";
    document.body.appendChild(el);;
    document.getElementById("test").disabled = true;


});
