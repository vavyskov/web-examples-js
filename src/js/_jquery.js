// Variable
var $, jQuery;

//$(document).ready(function () {
$(function () { // Short equivalent of "$(document).ready" method

    /* Carousel
    ============================================================ */
    $('.carousel').carousel({
        //interval: 2000,
        interval: false
    });

    /* Colorbox
    ============================================================ */
    //jQuery('a.gallery').colorbox();
    jQuery('a.gallery').colorbox({
        opacity:0.5,
        rel:'group1',
        scalePhotos:true,
        maxHeight: "90%",
        maxWidth: "90%",
        current: "Obrázek {current}/{total}",
        loop: false,
        slideshow: true,
        slideshowSpeed: 3000,
        slideshowStart: "Spustit",
        slideshowStop: "Zastavit",
    });
});