// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';



    /**
     * Kontext události
     * =========================================================================
     */

    const  demo = document.getElementById("demo");

    demo.onclick = function (e) {
    };

    demo.addEventListener("click", function (e) {
        // Zjištění na kterém prvku událost nastala
        console.log(this);

        // Informace o aktivační události (parametr e reprezentuje objekt události)
        console.log(e);
    });

    /**
     * Mouse position
     */
    window.onmousemove = function (e) {
        //console.log(e.clientX + ", " + e.clientY + " (" + e.screenX + ", " + e.screenY + ")");
    };

    /**
     * Keyboard press detection (textarea)
     */
    let textarea = document.getElementById("textarea");

    textarea.onkeypress = function (e) {
    };

    textarea.addEventListener('keydown', (e) => {
        console.log("Textarea: " + e.key);
    });

    /**
     * Keyboard press detection (window)
     */
    window.onkeypress = function (e) {

    };

    window.addEventListener('keydown', (e) => {
        // Deprecated
        // fromCharCode - funkce pro převod kódu na znak
        // e.charCode - vlastnost objektu události e obsahující kód stisknuté klávesy
        //console.log(String.fromCharCode(e.charCode));

        // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
        //console.log(e.key);

        console.log(`Global key "${e.key}" pressed  [event: keydown]`);


        if (e.key === "F1") {
            alert("Nápověda");
        }
        if (e.key.toUpperCase() === "A") {
            alert(`Znak ${e.key} (malé "a" nebo velké "A")`);
        }
        /*var x = e.key;
        // If the pressed keyboard button is "a" or "A" (using caps lock or shift), alert some text.
        if (x === "a" || x === "A") {
            alert ("You pressed the 'A' key!");
        }*/

    });

});
