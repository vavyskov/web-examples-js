// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     * Zpráva úkolů
     * =========================================================================
     */

    // Constants
    const demo = document.getElementById('demo');

    // Definice vlatní události UkolPridan
    const udalostUkolu = new CustomEvent('UkolPridan', {
        detail: {
            zprava: "úkol přidán"
        },
        bubbles: true,
        cancelable: true
    });

    // Spuštění vlastní události metodou dispatchEvent()
    demo.onclick = function (e) {
        document.dispatchEvent(udalostUkolu);
    };



});
