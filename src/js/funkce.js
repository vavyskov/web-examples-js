// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    //function secti(a, b){
    function secti(){

        let sum = 0;

        /**
         * Version A
         * =========================================================================
         */
        /*for (let i = 0; i < arguments.length; i += 1) {
            sum += arguments[i];
        }*/

        /**
         * Version B
         * =========================================================================
         */
        // https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Functions/arguments
        // Objekt arguments není pole, ale může být na pole převeden!
        // Je podobný poli, ale nemá žádnou z vlastností pole, kromě délky (length).
        Array.from(arguments).forEach(function (item) {
            sum += item;
        });

        /**
         * Version C
         * =========================================================================
         */
        /*Array.from(arguments).forEach(function (item, index) {
            sum += item;
            console.log("index[" + index + "]: " + item);
        });*/

        /**
         * Version D
         * =========================================================================
         */
        /*//Array.prototype.slice.call(arguments).forEach(function (item) {
        [].slice.call(arguments).forEach(function (item) {
        //[].slice(arguments).forEach(function (item) {
            sum += item;
        });*/

        return sum;
    }

    // User event
    // https://www.w3schools.com/js/js_htmldom_eventlistener.asp
    document.getElementById('demo').addEventListener('click', function () {

        let vysledek;
        vysledek = secti(1, 2, 7, 12);

        // Write text
        // https://www.w3schools.com/jsref/prop_html_innerhtml.asp
        // https://www.w3schools.com/jsref/prop_node_innertext.asp
        document.getElementById('soucet').innerHTML = vysledek;
    });

    /**
     * Součet sudých čísel - verze A
     * =========================================================================
     *//*
    let cisla = [7, 10, 6, 1, 22, 13];
    let soucet = 0;
    for (let i = 0; i < cisla.length; i++) {
        if (cisla[i] % 2 === 0) {
            soucet += cisla[i];
        }
    }
    console.log(soucet);*/

    /**
     * Součet sudých čísel - verze B
     * =========================================================================
     */
    let cisla = [7, 10, 6, 1, 22, 13];
    let soucet = cisla.filter(function (x) {
        return x % 2 === 0;
    }).reduce(function (x, y) {
        return x + y;
    });
    console.log(soucet);

    /**
     * Capitalize - první písmeno velké
     * @return string
     * =========================================================================
     */
    const capitalize = function (s) {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    };
    console.log(capitalize('lorem ipsum'));
    // console.log(capitalize('lorem'));
    // console.log(capitalize('l'));
    // console.log(capitalize(0));
    // console.log(capitalize({}));

});
