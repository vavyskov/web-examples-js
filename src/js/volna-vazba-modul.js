// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     * Zpráva úkolů - rozšiřující modul
     * =========================================================================
     */

    function zpracujPridaniUkolu(e) {
        //alert(e.detail.zprava);
        document.getElementById("ukoly").innerHTML += `<li>${e.detail.zprava}</li>`;
    }

    document.addEventListener("UkolPridan", zpracujPridaniUkolu, false);

});
