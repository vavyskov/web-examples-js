// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     * Canvas
     * https://www.chartjs.org/
     * =========================================================================
     */

    // Constants
    const demo = document.getElementById('demo');
    const platno = document.getElementById('platno');
    const ulozit = document.getElementById('ulozit');

    if (platno && platno.getContext) {
        let kontext = platno.getContext("2d"); // 2D grafika
        //let kontext = platno.getContext("webgl"); // 3D grafika

        // Výplň
        kontext.fillStyle = '#eeeeee';
        kontext.fillRect(0, 0, platno.width, platno.height);

        // Okraj
        kontext.strokeStyle = '#333333';
        kontext.strokeRect(0, 0, platno.width, platno.height);

        // Nadpis
        let nadpis = "Nadpis grafu";
        let vyskaNadpisu = 20;
        kontext.fillStyle = "blue";
        kontext.font = "italic " + vyskaNadpisu + "px Times New Roman";
        //kontext.textAlign = "left";
        kontext.textAlign = "center";
        //kontext.textBaseline = "bottom";
        kontext.textBaseline = "middle";
        kontext.fillText(nadpis, platno.width/2, 15);
        let sirkaNadpisu = kontext.measureText(nadpis).width;
        console.log("Šířka nadpisu: " + sirkaNadpisu);
        console.log("Výška nadpisu: " + vyskaNadpisu);

        // Font
        kontext.font = "normal 10px Arial";

        // Nastavení vnitního odsazení
        let padding = 30;
        let width = platno.width - padding * 2;
        let height = platno.height - padding *2;
        // Nastavení počátku souřadnicového systému
        kontext.translate(padding, platno.height - padding);

        // Osa X
        kontext.fillStyle = 'blue';
        kontext.fillText('Osa X', width / 2, 20);

        // Osa Y
        kontext.fillStyle = 'blue';
        // Uložení aktuálního stavu kontextu (uložit je možné i více stavů)
        kontext.save();
        // Otočení souřadnicového systému o 90 stupňů (funkce vyžaduje hodnotu v radiánech)
        kontext.rotate(Math.PI * -90 / 180);
        kontext.fillText('Osa Y', height / 2, -20);
        // Obnovení počátečního stavu kontextu
        kontext.restore();

        // Obdelník 1
        let w1 = 30; // %
        let h1 = 70; // %
        kontext.fillStyle = 'orange';
        // Natočení
        //kontext.rotate(-10 * Math.PI / 180);
        kontext.fillRect(0, 0, width/100 * w1, height/100 * -h1);
        kontext.fillText("Obdelník 1", 0 + width/100 * w1 / 2, height/100 * -h1 - 5);

        // Měřítko osy X
        kontext.fillStyle = 'black';
        let maxX = 100;
        let krokX = 5;
        for (let i = 0; i <= maxX; i += maxX/krokX) {
            let souradniceX = i * width/maxX;
            kontext.fillText(Math.round(i), souradniceX, 6);

            // Mřížka
            // http://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
            kontext.moveTo(souradniceX - 0.5, 0);
            kontext.lineTo(souradniceX - 0.5, -height);
            kontext.lineWidth = 1;
            kontext.strokeStyle = "#cccccc";
            kontext.stroke();
        }

        // Měřítko osy Y
        let maxY = 10;
        let krokY = 5;

        // Uložení aktuálního stavu kontextu (uložit je možné i více stavů)
        kontext.save();
        // Otočení souřadnicového systému o 90 stupňů (funkce vyžaduje hodnotu v radiánech)
        kontext.rotate(Math.PI * -90 / 180);

        for (let i = 0; i <= maxY; i += maxY/krokY) {
            let souradniceY = i * height/maxY;
            kontext.fillText(Math.round(i), souradniceY, -6);

            // Mřížka
            // http://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
            kontext.moveTo(souradniceY - 0.5, 0);
            kontext.lineTo(souradniceY - 0.5, width);
            //kontext.lineWidth = 1;
            //kontext.strokeStyle = "#cccccc";
            kontext.stroke();
        }

        // Obnovení počátečního stavu kontextu
        kontext.restore();

        // Obdelník 2
        let w2 = 40; // %
        let h2 = 70; // %
        let x2 = 40; // %
        // Přechod
        let prechod = kontext.createLinearGradient(width/100 * w2, 0, width/100 * x2 + width/100 * w2, 0);
        prechod.addColorStop(0, "red");
        prechod.addColorStop(1, "blue");
        kontext.fillStyle = prechod;
        //kontext.fillStyle = 'green';
        kontext.fillRect(width/100 * x2, 0, width/100 * w2, height/100 * -h2);
        //kontext.fillText("Obdelník 2", width/100 * w2, height/100 * -h2);
        kontext.fillText("Obdelník 2", width/100 * x2 + width/100 * w2 / 2, height/100 * -h2 - 5);

        // Stín
        kontext.shadowOffsetX = 2;
        kontext.shadowOffsetY = 2;
        kontext.shadowBlur = 2;
        kontext.shadowColor = "rgba(0, 0, 0, 0.5)"

        // Oblouk
        kontext.beginPath();
        kontext.arc(80, -50, 30, 0, Math.PI * 1.5, false);
        kontext.strokeStyle = "green";
        kontext.stroke();

    }

    // Write text
    // https://www.w3schools.com/js/js_functions.asp
    function nakresli(data) {
        // Změna výšky nebo šířky plátna vymaže obsah plátna
        platno.width = platno.width;
    }

    // User event
    demo.addEventListener('click', nakresli);


    /**
     * Test podpory zvolené funkce nebo metody v prohlížeči
     * =========================================================================
     */
    function test() {
        return false;
    }

    // Zavolání a vyhodnoscení výstupu funkce
    if (test()) {
        console.log("Test funkce SE závorkami.");
    }

    // Test existence funkce nebo metody
    if (test) {
        console.log("Test funkce BEZ závorek.");
    }



});
