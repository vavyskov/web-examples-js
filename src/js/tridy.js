// DOM loaded (without externals objects)
window.addEventListener('DOMContentLoaded', () => {

    // Strict mode
    // https://www.w3schools.com/js/js_strict.asp
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    /**
     * Version A
     * =========================================================================
     *//*
    var bezny, sporici;

    // Deklarace funkce Účet (obdoba definice třídy Účet) - konstruktor
    // Dohodnutá konvnce - běžná funkce začíná malým písmenem, funkce plnícít roli třídy začíná velkým písmenem
    function Ucet(cisloUctu) {
        // Vlastnost číslo účtu
        this.cisloUctu = cisloUctu;
        // Vlastnost zůstatek
        this.zustatek = 0;

        // Metoda vlož finanční prostředky
        this.vloz = function (castka) {
            this.zustatek += castka;
        };

        // Metoda vrať zůstatek
        this.vratZustatek = function () {
            return this.zustatek;
        }
    }

    // Vytvoření instance běžný
    // Do proměnné "běžný" vložíme pomocí konstruktoru "new" novou instanci funkce (objektu) Účet
    bezny = new Ucet("123456789");
    sporici = new Ucet(987654321);

    // Metodou vlož přidáme na účet volitelnou částku
    bezny.vloz(100);
    sporici.vloz(200);

    // Voláním metody vrať zůstatek zjistíme zůstatek na požadovaném účtu
    console.log("Zůstatek účtu " + bezny.cisloUctu + " je " + bezny.vratZustatek() + ",- Kč.");
    console.log("Zůstatek účtu " + sporici.cisloUctu + " je " + sporici.vratZustatek() + ",- Kč.");

    /**
     * Version B
     * =========================================================================
     */
    let bezny, sporici;

    // Deklarace funkce Účet (obdoba definice třídy Účet) - konstruktor
    // Dohodnutá konvnce - běžná funkce začíná malým písmenem, funkce plnícít roli třídy začíná velkým písmenem
    class Ucet {

        constructor (cisloUctu) {
            // Vlastnost číslo účtu
            this.cisloUctu = cisloUctu;
            // Vlastnost zůstatek
            this.zustatek = 0;
        }

        // Metoda vlož finanční prostředky
        vloz (castka) {
            this.zustatek += castka;
        }

        // Metoda vrať zůstatek
        vratZustatek() {
            return this.zustatek;
        }
    }

    // Vytvoření instance běžný
    // Do proměnné "běžný" vložíme pomocí konstruktoru "new" novou instanci funkce (objektu) Účet
    bezny = new Ucet("123456789");
    sporici = new Ucet(987654321);

    // Metodou vlož přidáme na účet volitelnou částku
    bezny.vloz(100);
    sporici.vloz(200);

    // Voláním metody vrať zůstatek zjistíme zůstatek na požadovaném účtu
    console.log("Zůstatek účtu " + bezny.cisloUctu + " je " + bezny.vratZustatek() + ",- Kč.");
    console.log("Zůstatek účtu " + sporici.cisloUctu + " je " + sporici.vratZustatek() + ",- Kč.");

});
