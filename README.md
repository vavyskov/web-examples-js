# web-examples-js

JavaScript příklady

Kontrola:
- Google Chrome:
  - DevTools, **Lighthouse** (spouštět v **anonymním okně** nebo vypnout všechna rozšíření)
    - [Node.js](https://nodejs.org/en/)
    - `npx http-server`
    - `npx http-server -c31536000` (cache time in seconds)
  - Rozšíření:
    - **Web Vitals**