'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    /**
     * ID
     */
    document.getElementById("osoby").style.border = "1px solid #343a40";

    var osoby = document.getElementById("osoby");

    //osoby.style.border = "1px solid red";
    //osoby.className = "table"; // Remove other classes!!!
    //osoby.className += " table"; // Require regular expressions (remove, toggle etc.)

    // IE10+ partial support of classList https://caniuse.com/#search=classList
    osoby.classList.add("table");
    osoby.classList.add("table-bordered");
    osoby.classList.remove("table-bordered");
    osoby.classList.toggle("table-bordered");

    //osoby.classList.remove("demo");
    if (osoby.classList.contains("demo")) {
        document.getElementById("osoby").style.borderCollapse = "separate";
    }



    /**
     * Tag Name
     */
    var th = document.getElementsByTagName("th");
    console.log("Počet buněk záhlaví tabulky: " + th.length);

    var td = document.getElementsByTagName("td");
    console.log("Počet buněk těla tabulky: " + td.length);



    /**
     * CSS Selector
     */
    document.querySelector("h1").style.color = "brown";
    document.querySelector("#osoby th").style.backgroundColor = "orange"; // Pouze prvni výskyt



    var vek = document.querySelectorAll("#osoby td:nth-child(4)");
    var prumernyVek = 0;

    //console.log(vek);
    function vekovyPrumer(item, index) {
        // console.log(index);
        // console.log(item);

        item.style.color = "green";

        //console.log(item.innerText);

        // prumernyVek = prumernyVek + item.innerText;
        // prumernyVek = prumernyVek + Number(item.innerText);
        // prumernyVek += Number(item.innerText);

        prumernyVek += Number(item.innerText) / vek.length;
    }
    vek.forEach(vekovyPrumer);
    console.log("Průměrný věk je: " + prumernyVek);



    /*
    console.log(td);
    //td.style.backgroundColor = "yellow"; // Nefunguje

    // Version A
    for (var i = 0; i < td.length; i++) {
        td[i].style.backgroundColor = "yellow";
    }

    // Version B
    function bg(element) {
        element.style.backgroundColor = "yellow";
    }
    // Convert HTMLCollection to an Array
    Array.prototype.forEach.call(td, bg);
    */



});
