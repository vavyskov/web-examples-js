'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    var btn, fn1, fn2, fn3;

    btn = document.getElementById("demo");

    fn1 = function () {
        alert("Tlačítko stisknuto.");
    };

    // ES5
    fn2 = function (event) {
        console.log("Kontext: " + this); // Nesprávný kontext this (odkazuje na objekt, který událost vyvolal)
        console.log("Bylo kliknuto na: " + event.target);
    };

    // ES6+
    fn3 = (event) => {
        console.log("Kontext: " + this); // Správný kontext this (všechny globální proměnné a funkce jsou definovány v kontextu objektu window)
        console.log("Bylo kliknuto na: " + event.target);
    };

    btn.addEventListener("click", fn1);
    btn.addEventListener("click", fn2);
    btn.addEventListener("click", fn3);



});
