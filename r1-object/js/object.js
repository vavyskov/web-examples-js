'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    /**
     * Pole - číselné indexy, hranaté závorky
     */
    var zelenina = [
        "mrkev",
        "rajče",
        "květák"
    ];
    console.log(zelenina);
    console.log(zelenina[1]);
    console.log("Počet položek pole 'zelenina': " + zelenina.length);

    console.log("Položka 'zelenina' je pole: " + Array.isArray(zelenina));
    console.log(zelenina instanceof Array);



    /**
     * Objekt - nečíselné indexy, složené závorky
     */
    var osoba = {
        jmeno: "Jan",
        prijmeni: "Novák",
        vek: 44
    };
    console.log(osoba);
    console.log(osoba["prijmeni"]);
    console.log(osoba.prijmeni);
    console.log("Počet vlastností objektu 'osoba': " + osoba.length); // Undefined!!!

    console.log("Položka 'osoba' je pole': " + Array.isArray(osoba));
    console.log(osoba instanceof Object);



    /* ---------------------------------------------------------------------------------------------------------------*/



    /**
     * Vytvoření objektu na základě objektu Object - jedna instance
     */
    var o1 = new Object();
    o1.vlastnost = "hodnota";
    o1.metoda = function () {};

    /**
     * Vytvoření objektu prostřednictvím objektu Initializer - jedna instance
     */

    // Vlastní instance metody

    var o2 = {
        vlastnost: "hodnota o2",
        metoda: function () {},
    };
    console.log(o2.vlastnost);

    // Sdílená instance metody

    var fn = function () { return this.vlastnost; };
    var o2a = {
        vlastnost: "hodnota o2a",
        metoda: fn,
    };
    var o2b = {
        vlastnost: "hodnota o2b",
        metoda: fn,
    };
    console.log(o2a.metoda());
    console.log(o2b.metoda());

    /**
     * Vytvoření objektu pomocí konstrukční funkce (Constructor Function) - více instancí
     */
    var Fn = function (a, b) {
        // Private
        var x = 0;
        // Public
        this.getX = function () {return x;};
        this.vlastnost = "hodnota Fn";
        this.a = a;
        this.b = b || "Výchozí text";
    };
    var o3 = new Fn(7, "hodnota o3");

    console.log(o3.x); // Undefined!!!
    console.log(o3.getX);
    console.log(o3.getX());
    console.log(o3.vlastnost);
    console.log(o3.b);

    /**
     * Vytvoření objektu pomocí metody Object.create()
     */



    /**
     * Vytvoření objektu pomocí Class (ES6+) - více instancí
     */



    // Notes:
    // https://www.itnetwork.cz/javascript/oop/javascript-tutorial-navrh-doplnku
    // https://gist.github.com/remarkablemark/fa62af0a2c57f5ef54226cae2258b38d
    // https://medium.com/@apalshah/javascript-class-difference-between-es5-and-es6-classes-a37b6c90c7f8
    // https://babeljs.io/repl (Presets: es2015 vs es2015-loose)

});
