'use strict';

// DOM (without externals objects) loaded
window.addEventListener('DOMContentLoaded', function() { // ES5
//window.addEventListener('DOMContentLoaded', () => { // ES6



    var btn, fn1, fn2;

    btn = document.getElementById("demo");

    fn1 = function () {
        alert("Tlačítko stisknuto.");
    };

    fn2 = function (event) {
        console.log("Bylo kliknuto na: " + event.target);
        console.log("Bylo kliknuto na tlačítko s textem: " + event.target.innerText);
    };

    btn.addEventListener("click", fn1);
    btn.addEventListener("click", fn2);



});
