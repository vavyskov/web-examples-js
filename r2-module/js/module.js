'use strict';

(function () {

    /**
     * Lokální proměnná
     */
    var text = "Lokální proměnná";

    /**
     * Lokální objekt
     */
    // ToDo: 3D kostka
    // CSS https://3dtransforms.desandro.com/cube
    // Canvas https://github.com/mrdoob/three.js => https://jsfiddle.net/f2Lommf5/ => https://threejs.org/examples/?q=cube#webgl_interactive_cubes_gpu => https://discoverthreejs.com/book/first-steps/textures-intro/

    // ToDo: Percentage Bar
    // https://developer.mozilla.org/en-US/docs/Web/SVG/Element/svg
    // https://www.cssscript.com/pure-css-circular-percentage-bar/

    // ToDo: SVG line aninmation
    // https://css-tricks.com/svg-line-animation-works/
    // https://scotch.io/tutorials/understanding-hoisting-in-javascript (SVG v nadpisech, SVG ohraničující ukázkový kód, postranní "čárkovaná" navigace)

    // ToDo: Timer (CountDown)
    // https://codepen.io/zebateira/pen/VvqJwm
    // https://www.cssscript.com/circular-countdown-timer-javascript-css3/
    // https://codepen.io/zsoltime/pen/zwRPZO
    // https://codepen.io/sergixnet/pen/QvzLEO






    /**
     * Globální objekt
     */
    window.myModule = {
        myText: text,
    };

})();
